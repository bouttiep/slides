---
marp: true
theme: gricad
paginate: true
footer: "COMUT 28/04/2021 - *pierre-antoine.bouttier@univ-grenoble-alpes.fr*"
---

# Actualités GRICAD

## Réunion du COMUT - 28 avril 2021

*Pierre-Antoine Bouttier*

![w:400 center](./fig/logo.png)

--- 
# De quoi vais-je parler ?

* **Perseus** **N**ext **G**eneration
* Évolutions des infras et services existants
* De nouvelles infrastructures/services

--- 

# Perseus-NG

---

## Perseus-NG, dans les grandes lignes 

- Notre système d'information pour accéder aux clusters (login, mdp, groupes, permissions, etc.)...
- ...Maintenant basé en majeure partie sur **AGALAN**
- [Un portail web](https://perseus.univ-grenoble-alpes.fr/) et [un wiki](https://perseus-wiki.univ-grenoble-alpes.fr/) pour gérer projets et comptes (administrateurs, reviewers et utilisateurs) relatifs à votre activité sur certains services GRICAD
- Accès aux serveurs HPC et bientôt NOVA (et d'autres services...)
- *Développé de A à Z par **Noémie et Thomas Coupechoux** (un grand merci et bravo à eux !)*

--- 

## Perseus NG en tant qu'utilisateur

* Utilisateur **non-permanent** : 
  * Contrat type *CDD* ou collaborateur extérieur à l'UI
  * Rejoindre des projets
  * Gestion de son compte, liens utiles (docs, assistance)
  
* Utilisateur **permanent** : 
  * Création et édition de projets conditionnant l'accès aux services 
  * Edition des rapports annuels et bibliographie sur le wiki pour chaque projet 
  * En tant que responsable de projet : responsable de l'identification (confiance) pour chaque membre de son projet

--- 

## Perseus NG en tant que rapporteur

* Chaque pôle thématique de l'UI à deux rapporteurs (technique et scientifique)
* Rôle : 
  * Valider les projets nouvellement créés
  * Valider les rapports annuels et la prolongation des projets
* **La consultation des rapports et les interactions** (questions, remarques) sont à faire sur [**le wiki des projets**](https://perseus-wiki.univ-grenoble-alpes.fr/)
* Validation et extension sur **Perseus NG**

---
## Perseus NG en *amélioration continue*

- Correction de bugs
- Ajouts de nouvelles fonctionnalités sur le portail web ET le wiki (*e.g.* DOI)
- Ajouts de nouveaux services
- Pour tout problème : sos-gricad@univ-grenoble-alpes.fr
---

# Evolutions des services/infras

---
## Froggy, le vénérable

- 132 noeuds sur les 190 répondent au ping
- De moins en moins utilisé
- Une fin de vie paisible

---
## Dahu, le fougueux

Dans la dernière année, ajout de : 
* 450 coeurs CPUs
* 3 noeuds 4 GPUs NVIDIA Tesla V100
* 1 noeud de visu
* 1 *fat node* 
* **Cluster en constante évolution, avec le souci de répondre à la grande hétérogénéité des usages**

---
## Luke, l'hétéroclite

* Historiquement, cluster hétérogène, avec noeuds communs, noeuds "d'équipe", noeuds particuliers.
* Noeuds d'équipes
  * Sous-utilisés pour certains 
  * Chronophages à maintenir
  * Problématique de place dans le DC
  * Remplaçables dans la grande majeure partie des cas par la versatilité de dahu
* **Diminution drastique de ce mode de fonctionnement**

---
## Le cloud computing, NOVA

* Beaucoup de travail en interne pour renforcer/sécuriser l'infrastructure et la développer à long terme
* De plus en plus utilisée
* Intégration de vGPU
* **Excellente plateforme de développement/test** : sos-nova-gricad@univ-grenoble-alpes.fr

---
# Les nouvelles infrastructures

--- 
# MANTIS V2

* Remplacement de MANTIS (10 ans) : stockage *Cloud* couplé aux clusters de calcul
* Stockage distribué, basé sur iRODS, accessible depuis les différents serveurs de calculs
* Volume global extensible de 750To
* *Réflexion en cours pour rendre cet espace plus versatile : consolidation pour données semi-froides, opérabilité depuis l'extérieur (IDRIS, autres mésocentres, etc)*

---
# Silenus, un scratch Dahu haute performance 

* **Constats sur /bettik** : 
  * Bettik très utilisé (et pas que pour du scratch)
  * Certaines types d'IO ralentissent l'infra
  * Nettoyage manuel
* => Mise en place d'un nouvel espace de stockage

---
# Silenus, un scratch Dahu haute performance 

![w:800 center](fig/silenus.png)

---
# Silenus, un scratch Dahu haute performance 

* Adapté aux jobs avec un fort taux d'IOPS
* Basé sur BeeGFS (comme **/bettik**), câblé par omnipath, 70To utiles
* Réellement un scratch (politique de nettoyage automatique en cours de réflexion)
* Plus de détails prochainement

---

# [Accounting et monitoring des plateformes HPC](https://gricad-dashboards.univ-grenoble-alpes.fr/d/8nXIw5XMz/oar-clusters-project-stats?orgId=1)

--- 

# One more thing...

--- 
# Eli, plateforme de fouille de données

![w:800 center](fig/eli.png)

---
# Eli, plateforme de fouille de données

* Développée pour l'accounting des plateformes...
* ...Mais *facilement* déployable pour analyser n'importe quel jeu de données !
* Ingestion des données possible par **/bettik**
* Déjà déployée pour un projet scientifique !
* **Un grand merci et bravo à B. Bzeznik et O. Henriot**

---

# Merci de votre attention !