---
marp: true
theme: gricad
paginate: true
footer: "JUG 2020 - *pierre-antoine.bouttier@univ-grenoble-alpes.fr*"
---

# Bilans et nouveautés GRICAD

## JUG 2020 - 2 novembre 2020

*Pierre-Antoine Bouttier*

![w:400 center](./fig/logo.png)

--- 

# Quelques rappels sur GRICAD

---

# GRICAD dans les (très) grandes lignes

## Les missions principales

* **Accompagnement, conseils et formations** aux chercheurs sur leurs besoins liés au calcul et aux données
* Mise à disposition de l’ensemble des chercheurs et personnels en support de la recherche d'**infrastructures avancées et mutualisées** pour le calcul intensif et l’exploitation des données de la recherche.

---

# L'organisation de l'UMS

![width:700px center](fig/organigramme-gricad.png)

---

# Outils numériques pour la recherche

* **CIMENT**, plateformes de calcul et espaces de stockage associés
* **NOVA**, plateforme de VM dynamiques NOVA 
* [**JupyterHub**](https://jupyterhub.u-ga.fr/), plateforme de notebooks
* [**Gitlab**](https://gricad-gitlab.univ-grenoble-alpes.fr/), plateforme collaborative de développement logiciel (*et plus si affinités*)

---

# Bilans & nouveautés

---

# CIMENT
## Les serveurs de calcul

* **Froggy**, une fin de vie qui dure
    * Mais pour combien de temps ?
* Le **Dahu** grandit
    * 31 noeuds ajoutés en 2020 (3192 coeurs, presque comme Froggy !)
    * Diversification des noeuds : répondre au plus grand nombre
    * Bientôt : noeuds GPU, fat nodes, noeud de visu, etc.

---

# CIMENT
## Les serveurs de stockage

* Bettik, espace de stockage haute performance
    - Un travail de configuration et stabilisation effectué
    - 1,3Po remplis à 80%
    
* La jouvence de MANTIS
    - Espace de stockage de données *tièdes* (iRods)
    - Une toute nouvelle infra (**MANTIS-2**) : 750Go disponibles, accessibles à tous
    - Bientôt, invitation à migrer vos données

---
# CIMENT
## Accounting

![w:640 center](fig/account_active_since_2015.png)

---
# CIMENT
## Accounting

![w:640 center](fig/project_active_since_2015.png)

---
# NOVA
## Qu'est-ce que c'est ?

* Création de VM à la demande, de façon dynamique
* Usages : environnement de développement/test, récolte de données, enseignement à distance
* 40 projets qui l'utilisent (x2 en 2020)

---
# NOVA
## L'infrastructure

* 13 noeuds *compute*, 312 coeurs (+6 coeurs par rapport à 2109)
* 384Go de RAM par noeuds
* 80To Ceph utiles (x2 par rapport à 2019)
* Liens réseaux 2x20g (au lieu de 4x10g)
* 1 noeud avec 2 GPU  Tesla v100 (partageable en 7). Bientôt un deuxième...

---

# Gitlab
## La plateforme de développement logiciel 

* Plus de 5000 projets, plus de 5000 utilisateurs (+1000 en 2020)
* Infrastructure :
    * 1 VM Winter
    * 5 TBs Summer 
    * 1 projet Nova (Vms pour les tests et l’intégration continue)

---

# La cellule "Data stewardship"

Pour accompagner la recherche grenobloise dans la gestion des données de la recherche.

*Cf. Présentation de  Violaine*

---

# À venir

--- 
# Perseus-ng
## Refonte du portail de gestion de comptes et de projet 

Pourquoi ? 

- Rationalisation du SI (LDAP maison vs LDAP agalan)
- Interface vieillissante
- Inclusion de nouveaux services (e.g. NOVA)

Pour quand ? 

**Le mois prochain !** (*nous risquons de traverser une zone de turbulence*)

---
# Perseus-ng
## Refonte du portail de gestion de comptes et de projet 

### Quoi de neuf ?

* Une nouvelle interface web super !
* Un nouveau LDAP synchro avec Agalan
* Une nouvelle interface de reporting super ! 

**Félicitations aux 2 vaillants développeurs, Noémie et Thomas Coupechoux !**

---

# Eli, plateforme de fouille de données

D'un besoin de monitoring est née une plateforme de fouille de données (Elastic Search + Grafana)

*Cf. présentation d'Oliver Henriot*

---

# Eli, plateforme de fouille de données

![h:540 center](fig/eli.png)

---

# Silenus
## Un nouvel espace de stockage haute performance

### Pourquoi ?

- Bettik limité en perfs pour les tâches qui font de nombreux accès directs aux fichiers, ou qui traitent de nombreux petits fichiers (>10000)
- De telles tâches pénalisent l'ensemble des utilisateurs 

---

# Silenus
### Le compagnon de bettik

- Nouvelle instance BeeGFS, basée uniquement sur SSD et NVMe, noeuds reliés en omnipath, centré sur les usages décrits ci-dessus
- 50To utiles visés à la mise en place
- À usage *scratch* exclusivement

---

# Merci de votre attention !

- Pour savoir comment utiliser nos services : https://gricad-doc.univ-grenoble-alpes.fr/
- Un problème, une question sur l'utilisation des services ? sos-gricad@univ-grenoble-alpes.fr
- Pour des demandes plus générales au sujet de GRICAD : gricad-contact@univ-grenoble-alpes.fr