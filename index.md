---
marp: true
theme: gricad
footer: "UMS GRICAD - *pierre-antoine.bouttier@univ-grenoble-alpes.fr*"
---
 
# P-A Bouttier slides

---
# 2021

[GRICAD computing resources overview - Grenoble R group - january 2021](./20210114_group-r.html) - [[pdf]](./20210114_group-r.pdf)
[SARI seminar *recherche reproductible* - April 2021](./20210409_rech_repro.html) - [[pdf]](./20210409_rech_repro.pdf)
[Réunion COMUT 28/04/2021](./COMUT_20210428.html) - [[pdf]](./COMUT_20210428.pdf)
[Rencontres Guix - mai 2021](./rencontres_guix_20210517.html) - [[pdf]](./rencontres_guix_20210517.pdf)
[RDA Software Source Code - 9th november 2021](./20211109_RDA_repro.html) - [[pdf]](./20211109_RDA_repro.pdf)
[Guix en pratique - Séminaire Recherche reproductible - 25 novembre 2021](./20211125_rech_repro.html) - [[pdf]](20211125_rech_repro.pdf)
[Journée utilisateur GRICAD - novembre 2021](./JUG_2021.html) - [[pdf]](JUG_2021.pdf)

---
# 2020
[Journées Utilisateur GRICAD - novembre 2020](./JUG.html)
[GT Architecture Data Terra - décembre 2020](./data_terra_gt_archi.html)

