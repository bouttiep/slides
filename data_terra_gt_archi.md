---
marp: true
theme: gricad
paginate: true
footer: "GT data terra - *pierre-antoine.bouttier@univ-grenoble-alpes.fr*"
---

# Gestion des environnements logiciels

## Retour d'expérience GRICAD

*Pierre-Antoine Bouttier*

![w:400 center](fig/logo.png)

--- 

# Notre contexte

* 3 clusters de calcul en propre (environ 10000 coeurs CPU)...
* ...reliés entre eux et à des clusters de labos par une grille de calcul local (*CiGri*)
* Utilisations : 
  *  HPC 
  *  Traitement de données
  *  HTC
  *  Visu, formation & développement
* **Grande hétérogénéité** des usages, des communautés, des niveaux de compétences utilisateur

---

# The Good Ol' days

* Jusqu'à 2015, utilisation de module
  * Classique, bien connu 
  * Utilisé au tiers-1
  * Usages et communautés très homogènes
  
* Mais des gênes se font sentir : 
  * Reproductiblité (très) moyenne (indispensable pour notre petite grille !)
  * Duplication des efforts proportionnelle au nombre de clusters
  * Aspect communautaire marginal (*e.g.* pas de partage direct de paquets)
  * ...

--- 

# L'utopie logicielle pour le HPC

* **Maintenance** (arbre des dépendances bien géré, système indépendant), **reproductibilité** (version unique d'un paquet - src, compilation, desc, déf,...- a la même sortie où que ce soit), **portabilité**
* **Gestion avancée des permissions** (environnement utilisateur, paquets/environnements spécifiques à une communauté, licences)
* **Worklow automatisé** : paquets *custom*, rebuilds automatiques, du PC perso aux clusters, CI
* Options de compilation avancée
* **OS-indépendant**  

---

# Welcome to the jungle (out-of-date?)

![w:1000 center](fig/jungle_softenv.png)

---

# Here comes a new challenger

* En développant une solution maison (à base de liens symboliques et une glibc embarquée...), Bruno Bzeznik est tombé sur **Nix** : 
  * Orienté reproductibilité
  * Très faible dépendance au système d'exploitation hôte
  * Communauté jeune mais active
  * En 2015, on (enfin *il*) se lance

--- 

# Nix, briefly

* Les paquets et environnements sont définis par une expression Nix (fichier source)
* OS-indépendant
* Mise à niveau ou rollbacks atomiques
* Plusieurs versions d'un même paquet pour le même utilisateur
* Pas de privilège pour installer les paquets
* Fourni des environnements **isolés** de construction et d'exécution 
* Reproducibilité à partir des sources
* Le binary cache
* Un garbage collector

---

# The dark side (1/3) 

## Côté utilisateur : 

* N'a aucun lien avec module. 
* Ligne de commande peu explicite
* ??? 

---

# The dark side (2/3)

## Côté développeurs/mainteneurs de paquets

* N'a aucun lien avec module
* Langage qui peut être déroutant
* Courbe d'apprentissage raide (peu de chances que l'utilisateur soit développeur)
* Parfois tricky d'empaqueter des binaires ou softs aux archi. compliquées

---

# The dark side (3/3)

## Côté ASR 

* N'a aucun lien avec module
* Manque le mode d'écoute TCP au nix-daemon (socat pour lancer nix-shell sur les noeuds)

---

# Pourquoi Guix alors ?

* Parce qu'on aime bien tester des trucs
* Parce que le Lead developer est sympa et français (L. Courtès)

---
# Guix VS Nix - Installation de paquets

### Nix

```Shell
$ nix-env -i gcc # Installe les compilos GNU
$ nix-env -q # Liste des paquets installés dans l'environnement courant 
gcc-9.2.0
$ nix-env -e gcc # Désinstalle le paquet
```

### Guix 

```Shell
$ guix install gcc-toolchain
$ guix package -I 
$ guix remove gcc-toolchain
```

---

# Guix VS Nix - Définition de paquets NIX

```Nix
{ stdenv, fetchurl }:

stdenv.mkDerivation rec {
  pname = "hello";
  version = "2.10";
  src = fetchurl {
    url = "mirror://gnu/hello/${pname}-${version}.tar.gz";
    sha256 = "0ssi1wpaf7plaswqqjwigppsg5fyh99vdlb9kzl7c9lng89ndq1i";
  };

  doCheck = true;

  meta = with stdenv.lib; {
    description = "A program that produces a familiar, friendly greeting";
    homepage = https://www.gnu.org/software/hello/manual/;
    changelog = "https://git.savannah.gnu.org/cgit/hello.git/plain/NEWS?h=v${version}";
    license = licenses.gpl3Plus;
    maintainers = [ maintainers.eelco ];
    platforms = platforms.all;
  };
}
```
---

# Guix VS Nix - Définition de paquets GUIX

```Scheme
(define-public hello
  (package
    (name "hello")
    (version "2.10")
    (source (origin
              (method url-fetch)
              (uri (string-append "mirror://gnu/hello/hello-" version
                                  ".tar.gz"))
              (sha256
               (base32
                "0ssi1wpaf7plaswqqjwigppsg5fyh99vdlb9kzl7c9lng89ndq1i"))))
    (build-system gnu-build-system)
    (synopsis "Hello, GNU world: An example GNU package")
    (description
     "GNU Hello prints the message \"Hello, world!\" and then exits.  It
serves as an example of standard GNU coding practices.")
    (home-page "https://www.gnu.org/software/hello/")
    (license gpl3+)))
```

---

# Guix VS Nix

* Interface utilisateur : avantage GUIX
* Définition de paquets : peut-être *léger avantage* guix
* Versatilité : Avantage Nix (gestion des profiles)
* Environnement python : avantage GUIX
* Compilation à la main : avantage GUIX 
* Communauté : match nul mais des différences
* Support MacOS: avantage Nix
* Préférence personnelle : GUIX (*Comment ça, ça se voit ?*)


---

# En conclusion

* Se passer de module se fait bien.
* Nous n'avons pas exploré suffisamment les autres solutions (Easybuild, Spack, Conda) pour vous donner un avis approfondi par rapport à Nix et Guix...
* ...Mais, sur le papier, sont mieux !
* Dans tous les cas, tenter de s'affranchir de **la dépendance au chemin**

--- 

# Merci de votre attention !

## Questions/Discussion